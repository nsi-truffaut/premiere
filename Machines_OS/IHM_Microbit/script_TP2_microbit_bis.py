from microbit import *
import music

personnes = 0
X = 0
Y = 0
i = 0

while True:
  if button_a.is_pressed():
    personnes = (personnes if isinstance(personnes, int or float) else 0) + 1
    if personnes == 10:
      display.set_pixel(X,Y,4)
      i = (i if isinstance(i, int or float) else 0) + 10
      personnes = 0
      X = (X if isinstance(X, int or float) else 0) + 1
      if X == 5:
        X = 0
        Y = (Y if isinstance(Y, int or float) else 0) + 1
  if button_b.is_pressed():
    music.play(['d:5'], pin=pin0)
    personnes = i + personnes
    display.show(str(personnes))
