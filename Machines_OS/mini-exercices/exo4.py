#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : exo4.py
#  auteur : Pascal CHAUVIN
#    date : 2021/01/25

#
# idee originale : IREM des Pays de la Loire
#

import ordinateur

### le programme a executer avec ses donnees

programme = [
	"RESET A", 
	"INC A", 
	"CMP A 36", 
	"JEQ 5", 
	"JUMP 1", 
	"RESET B", 
	"INC B", 
	"CMP B 37", 
	"JEQ 10", 
	"JUMP 6", 
	"CMP A 35", 
	"JEQ 17", 
	"CMP B 35", 
	"JEQ 24", 
	"DEC A", 
	"DEC B", 
	"JUMP 10", 
	"RESET B", 
	"INC B", 
	"CMP B 37", 
	"JEQ 22", 
	"JUMP 18", 
	"STO B 38", 
	"STOP", 
	"RESET B", 
	"INC B", 
	"CMP B 36", 
	"JEQ 29", 
	"JUMP 25", 
	"STO B 38", 
	"STOP", 
	"", 
	"", 
	"", 
	"", 
	"0", 
	"7", 
	"4", 
	"", 
	"", 
	"", 
	""
]

ordinateur.executer(programme)

# fin du fichier "exo4.py"
