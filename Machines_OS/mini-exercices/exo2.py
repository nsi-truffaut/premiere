#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : exo2.py
#  auteur : Pascal CHAUVIN
#    date : 2021/01/25

#
# idee originale : IREM des Pays de la Loire
#

import ordinateur

### le programme a executer avec ses donnees

programme = [
	"RESET A", 
	"INC A", 
	"CMP A 23", 
	"JEQ 5", 
	"JUMP 1", 
	"STO A 24", 
	"RESET C", 
	"RESET B", 
	"RESET A", 
	"INC A", 
	"INC B", 
	"CMP A 23", 
	"JEQ 14", 
	"JUMP 9", 
	"INC C", 
	"CMP C 24", 
	"JEQ 18", 
	"JUMP 8", 
	"STO B 25", 
	"STOP", 
	"", 
	"", 
	"", 
	"7", # donnee n°1
	"4", # donnee n°2
	"", 
	"", 
	"", 
	""
]

ordinateur.executer(programme)

# fin du fichier "exo2.py"
