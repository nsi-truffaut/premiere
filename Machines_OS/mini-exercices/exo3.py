#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : exo3.py
#  auteur : Pascal CHAUVIN
#    date : 2021/01/25

#
# idee originale : IREM des Pays de la Loire
#

import ordinateur

### le programme a executer avec ses donnees

programme = [
	"JUMP 5", 
	"7", 
	"4", 
	"25", 
	"", 
	"RESET B", 
	"RESET A", 
	"INC A", 
	"INC B", 
	"CMP A 1", 
	"JEQ 12", 
	"JUMP 7", 
	"RESET A", 
	"INC A", 
	"INC B", 
	"CMP A 2", 
	"JEQ 18", 
	"JUMP 13", 
	"RESET A", 
	"INC A", 
	"INC B", 
	"CMP A 3", 
	"JEQ 24", 
	"JUMP 19", 
	"STO B 4", 
	"STOP", 
	"", 
	""
]

ordinateur.executer(programme)

# fin du fichier "exo3.py"
