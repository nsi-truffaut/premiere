#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : mini.py
#  auteur : Pascal CHAUVIN
#    date : 2021/01/17

#
# idee originale : IREM des Pays de la Loire
#

import ordinateur

### le programme a executer avec ses donnees

programme = [
	"RESET C", 
	"RESET B", 
	"RESET A", 
	"INC A", 
	"INC B", 
	"CMP A 17", 
	"JEQ 8", 
	"JUMP 3", 
	"INC C", 
	"CMP C 18", 
	"JEQ 12", 
	"JUMP 2", 
	"STO B 19", 
	"STOP",
	"",
	"", 
	"", 
	"7", # donnee n°1
	"4", # donnee n°2
	""
]

ordinateur.executer(programme)

# fin du fichier "mini.py"
