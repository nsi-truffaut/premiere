#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : ordinateur.py
#  auteur : Pascal CHAUVIN
#    date : 2021/01/17


#
# idee originale : IREM des Pays de la Loire
#


def executer(programme):

	### MEMOIRE_MAXI : taille de la memoire vive (en lignes)

	MEMOIRE_MAXI = 32
	memoire = ["" for i in range(MEMOIRE_MAXI)]


	### chargement du programme et des donnees dans la memoire de travail (RAM)

	for i in range(len(programme)):
		memoire[i] = programme[i]
		

	### un registre pour chacun des compteurs A, B et C

	reg = {}

	reg["A"] = None # la valeur du registre est indeterminee en debut d'execution
	reg["B"] = None
	reg["C"] = None


	### execution du programme

	comparaison = None # valeur de la derniere instruction CMP

	ordinal = 0 # compteur ordinal (prochaine instruction a executer)

	while True: # boucle "infinie"

		instruction = memoire[ordinal]
		ordinal += 1
		
		xxx = instruction.split(" ")
		code = str(xxx[0]) # MNEMONIQUE (code de l'instruction)
		
		
		if code == "CMP":
			registre = str(xxx[1])
			adresse = eval(xxx[2])
			comparaison = (reg[registre] == eval(memoire[adresse]))
			continue


		if code == "INC":
			registre = str(xxx[1])
			reg[registre] += 1
			continue
			

		if code == "JEQ":
			adresse = eval(xxx[1])
			if comparaison:
				ordinal = adresse
			continue
					

		if code == "JUMP":
			adresse = eval(xxx[1])
			ordinal = adresse
			continue
				

		if code == "RESET":
			registre = str(xxx[1])
			reg[registre] = 0
			continue
			

		if code == "STO":
			registre = str(xxx[1])
			adresse = eval(xxx[2])
			memoire[adresse] = int(reg[registre])
			continue
				

		if code == "STOP":
			for n in reg:
				print("{} : {}".format(n, reg[n]))
			print("---")
			for i in range(len(memoire)):
				print("{:03} : {}".format(i, memoire[i]))
			print("fin du programme")
			quit()


		# fin du corps de la boucle "while"
	
	# fin de la fonction "executer()"
	
# fin du fichier "ordinateur.py"
