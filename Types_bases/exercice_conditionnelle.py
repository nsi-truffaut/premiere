def temperature(t) :
    """ Fonction qui donne l'état de l'eau suivant la température

        >>> temperature(-7)
        'solide'
        >>> temperature(35)
        'liquide'
        >>> temperature(123)
        'gazeux'
    """
    if t < 0 :
        return 'solide'
    elif t >= 0 and t < 100 :
        return 'liquide'
    else :
        return 'gazeux'

def dif_17(nombre) :
    """ Fonction qui retourne ce qu'on lui demande

        >>> dif_17(21)
        8
        >>> dif_17(5)
        12
    """

    if nombre < 17 :
        return 17 - nombre
    else :
        return 2*abs(17 - nombre)

def politesse(nom,sexe) :
    """ FOnction qui ajoute cher monsieur ou cher madame

        >>> politesse("Martin","M")
        'Cher Monsieur Martin'
        >>> politesse("Jade","F")
        'Chère Madame Jade'

    """

    if sexe == "M" :
        return "Cher Monsieur " + nom
    else :
        return "Chère Madame " + nom


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True) 

    
        
    
