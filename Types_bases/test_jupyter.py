def test_qcm1() :
    k=1
    rep = int(input("Quelle est votre réponse ?"))
    while k < 2 and rep !=15 :
        print("Réponse incorrecte")
        k = k+1
        rep = int(input("Quelle est votre réponse ?"))
    if k < 2 :
        print("Réponse correcte")
    else :
        print("Demandez de l'aide")
        
def test_qcm2() :
    k=1
    rep = int(input("Quelle est votre réponse ?"))
    while k < 2 and rep !=10 :
        print("Réponse incorrecte")
        k = k+1
        rep = int(input("Quelle est votre réponse ?"))
    if k < 2 :
        print("Réponse correcte")
    else :
        print("Demandez de l'aide")              

def test_question3() :
    k=1
    a = int(input("Quelle est la valeur de a ?"))
    b = int(input("Quelle est la valeur de b ?"))
    while k < 2 and (a !=5 and b !=8) :
        print("Réponse incorrecte")
        k = k+1
        a = int(input("Quelle est la valeur de a ?"))
        b = int(input("Quelle est la valeur de b ?"))
    if k < 2 :
        print("Réponse correcte")
    else :
        print("Demandez de l'aide")   
