#!/usr/bin/env python3


def calcul(a, b):
	"""
	Calcul de la somme de tous les entiers naturels compris entre a et b 
	(a et b inclus) et a plus petit que b.
	"""

	assert (a <= b), "a doit être inférieur ou égal à b."
	
	if a == b:
		return a
	else:
		return a + calcul(a + 1, b)


print(calcul(10, 13))

