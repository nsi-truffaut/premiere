#!/usr/bin/env python3


def calcul(a, b):
	"""
	Calcul de la somme de tous les entiers naturels compris entre a et b 
	(a et b inclus) et a plus petit que b.
	"""

	assert (a <= b), "Erreur : a doit être inférieur ou égal à b."
	
	somme = 0
	for n in range(a, (b + 1)):
		somme = somme + n
	
	return somme


print(calcul(10, 13))

