#!/usr/bin/env python3


def truc(x, phrase, i):
	if i == len(phrase):
		return 0
	else:
		if phrase[i] == x:
			return (1 + truc(x, phrase, i + 1))
		else:
			return truc(x, phrase, i + 1)


if __name__ == "__main__":
	message = "La phrase contient une seule lettre A."
	nombre_E = truc('e', message, 0)
	print(nombre_E)
	
