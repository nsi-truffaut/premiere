
let rec calcul a b =

	(*
	Calcul de la somme de tous les entiers naturels compris entre a et b 
	(a et b inclus) et a plus petit que b.
	*)

	if a > b then raise (Invalid_argument "a doit être inférieur ou égal à b.");
	
	if a = b then a 
	else (a + calcul (a + 1) b);;


print_int (calcul 10 13);;
print_endline "\n";;

