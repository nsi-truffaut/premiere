#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


int calcul(int a, int b) {
	int somme;
	
	assert (a <= b);
	
	somme = 0;
	
	for (int n = a; n <= b; n++) {
		somme = somme + n;
	}
	
	return somme;
}


int main(int argc, char **argv) {
	printf("%d\n", calcul(10, 13));
	return 0;
}

