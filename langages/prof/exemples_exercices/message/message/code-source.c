/*
 * fichier : code-source.c
 *  auteur : Pascal Chauvin et Gregory Maupu
 *    date : 2021-03-23
 */


#include "stdlib.h"
#include "stdio.h"


int truc(char x, char *phrase) { // ceci est un commentaire en fin de ligne
	int n;
	
	n = 0;
	for (int i = 0; phrase[i] != 0; i++) {
		if (phrase[i] == x) {
			n = n + 1;
		}
	}
	return n;
}


int main(int argc, char **argv) {
	char *message = "La phrase contient une seule lettre A.";
	
	int nombre_E;
	
	nombre_E = truc('e', message);
	
	printf("%d\n", nombre_E);
	
	return 0;
}
