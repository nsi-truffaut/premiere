#!/usr/bin/env python3


def truc(x, phrase):
	n = 0
	for i in range(len(phrase)):
		if phrase[i] == x:
			n = n + 1
	return n


if __name__ == "__main__":
	message = "La phrase contient une seule lettre A."
	nombre_E = truc('e', message)
	print(nombre_E)

