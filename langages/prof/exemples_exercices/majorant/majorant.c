/*
 * fichier : majorant.c
 *  auteur : Pascal Chauvin et Gregory Maupu
 *    date : 2021-03-23
 */


#include "stdlib.h"
#include "stdio.h"


int majorant(float x) {
	int n;
	n = 1;
	while (n < x) {
		n = n * 2;
	}
	return n;
}


int main(int argc, char **argv) {
	printf("Mon premier programme en langage C : que calcule-t-il ?\n");
	printf("%d\n", majorant(100.78));
	
	return 0;
}
