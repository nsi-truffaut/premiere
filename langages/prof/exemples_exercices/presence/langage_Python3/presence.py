#!/usr/bin/env python3


def truc(x, phrase, i):
	if i == len(phrase):
		return 0
	else:
		if phrase[i] == x:
			return (1 + truc(x, phrase, i + 1))
		else:
			return truc(x, phrase, i + 1)


def presence(x, phrase):
	if truc(x, phrase, 0) == 0:
		return False
	else:
		return True


if __name__ == "__main__":
	message = "La phrase contient une seule lettre A."
	print(presence('e', message))
	
