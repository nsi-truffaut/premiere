/*
 * fichier : prog.c
 *  auteur : Pascal Chauvin et Gregory Maupu
 *    date : 2021-03-23
 */


#include "stdlib.h"
#include "stdio.h"


int truc(char x, char *phrase) { // ceci est un commentaire en fin de ligne
	int n;
	
	n = 0;
	for (int i = 0; phrase[i] != 0; i++) {
		if (phrase[i] == x) {
			n = n + 1;
		}
	}
	return n;
}


int presence(char x, char *phrase) {
	int n;
	
	n = truc(x, phrase);
	if (n == 0) {
		return 0;
	} else {
		return 1;
	}
}


int main(int argc, char **argv) {
	char *message = "La phrase contient une seule lettre A.";
	
	printf("%d\n", presence('e', message));
	
	return 0;
}
