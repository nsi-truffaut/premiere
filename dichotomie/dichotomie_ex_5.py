#
# fichier : dichotomie_ex_5.py 
#  auteur : Grégory Maupu
#    date : 2021-05-25
#

def juste_nombre(n,a,b):
    reponse ='N'
    fini = False
    while reponse != 'O' and not fini :
        rep = (a+b)//2
        print('Le nombre choisi est-il ', rep)
        reponse = input('La réponse est-elle correcte ? (O/N) ')
        if reponse == 'O' :
            fini = True
        else :
            r = input('Le nombre choisi est-il supérieur ou inférieur à ma proposition ? (+/-)')
        if r == '+' :
            a = rep +1
        else :
            b = rep - 1
    return rep

if __name__=='__main__' :
    n = input('Quel est le nombre choisi ?')
    print(juste_nombre(n,0,100))

#fin du fichier dichotomie_ex_5.py 
