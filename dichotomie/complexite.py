import time
from random import *
from matplotlib import pyplot as plt
from pylab import *
from math import *

def algorithme_naif(L,e) :
    """Renvoie le premier indice de l'élément e dans L
    
    >>> algorithme_naif([2,8,45,101,23],23)
    4
    >>> algorithme_naif([-6,4,-10,3,58,1],4)
    1
    """
    
    present = False
    c = 0
    for i in range(len(L)) :
        c = c + 1
        if L[i] == e :
            c = c+1
            present = True
    return c

def dichotomie(L,e) :
    """ Teste la présence d'une valeur e dans une liste triée par ordre croissant L
    
    >>> dichotomie([1,2,3,4,5,6,7],6)
    True
    >>> dichotomie([-2,5,11,14,38,49],20)
    False
    """
    debut = 0
    fin = len(L)-1
    present = False
    c = 3
    while debut < fin and not present :
        
        milieu = (debut + fin) // 2
        c = c + 6
        if L[milieu] == e :
            present = True
            c = c + 2
        else :
            if L[milieu] > e :
                fin = milieu - 1
                c = c + 3
            else :
                debut = milieu + 1
                c = c + 3
    return c


def liste_aleatoire(n):
    return [randint(1,500) for __ in range(n)]

##def temps_execution(f,L,e):
##    t1 = time.time()
##    f(L,e)
##    t2 =time.time() - t1
##    return t2

def graphique(n):
    abs_liste =[k for k in range(1,n)]
    ord_liste=[]
    ord_liste2=[]
    for abscisse in abs_liste :
        L = liste_aleatoire(abscisse)
        ord_liste.append(dichotomie(L,20))
        ord_liste2.append(algorithme_naif(L,20))
    return abs_liste,ord_liste,ord_liste2

fig,ax = plt.subplots()
n=100
x = linspace(1,n,1)
xlim(0,n)

def show_graphique() :
    xdata,ydata,ydata2= graphique(n)[0],graphique(n)[1],graphique(n)[2]
    plt.plot(xdata,ydata,'.')
    plt.plot(xdata,ydata2,'.')
    plt.show()

def f(x) :
    return 8*log2(x)

def show_graphique_2() :
    xdata,ydata,ydata2= graphique(n)[0],graphique(n)[1],graphique(n)[2]
    plt.plot(xdata,ydata,'.')
    plt.plot(xdata,ydata2,'.')
    plt.plot(xdata,xdata)
    ydata3=list(map(f,xdata))
    plt.plot(xdata,ydata3 )
    plt.show()




