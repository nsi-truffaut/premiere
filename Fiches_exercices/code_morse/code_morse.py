
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : code_morse.py
#  auteur : Grégory Maupu
#    date : 2021/02/18

# Importation des modules

from son import jouer_son
from time import sleep

# Correspondance texte vers code morse

alphabet = {'A' : '.-',
            'B' : '-...',
            'C' : '-.-.',
            'D' : '-..',
            'E' : '.',
            'F' : '..-.',
            'G' : '--.',
            'H' : '....',
            'I' : '..',
            'J' : '.---',
            'K' : '-.-',
            'L' : '.-..',
            'M' : '--',
            'N' : '-.',
            'O' : '---',
            'P' : '.--.',
            'Q' : '--.-',
            'R' : '.-.',
            'S' : '...',
            'T' : '-',
            'U' : '..-',
            'V' : '...-',
            'X' : '-..-',
            'Y' : '-.--',
            'Z' : '--..',
            ' ' : ' '}

# Programme qui convertit un texte en majuscules vers le code morse

def texte_vers_morse(chaine):
    """ Le programme prend en entrée une chaîne de caractère en majuscule et renvoie
        la liste des codes morse associés à chaque lettre

        >>> texte_vers_morse("LA RE")
        ['.-..', '.-', ' ', '.-.', '.']
        >>> texte_vers_morse("OPEN")
        ['---', '.--.', '.', '-.']
    """
    morse=[]
    for car in chaine :
        morse.append(alphabet[car])
    return morse

# Programme qui inverse les clés et les valeurs d'un dictionnaire

def reverse_dict(dico) :
    """ Programme qui à partir d'un dictionnaire renvoie un dictionnaire où
        les clés et les valeurs sont échangés

        >>> reverse_dict({'Jean' : 3 , 'Prix' : 2.5})
        {3: 'Jean', 2.5: 'Prix'}
    """
    reverse_dico={}
    for cle in dico.keys():
        reverse_dico[dico[cle]] = cle
    return reverse_dico

# Programme qui transforme le code morse en son joué

def son_morse(morse) :
    for i in range(len(morse)) :
        for symbole in morse[i] :
            if symbole=="." :
                jouer_son('bip_court.wav')
                sleep(1)
            elif symbole=="-" :
                jouer_son('bip_long.wav')
                sleep(1)
            else :
                sleep(5)

# Programme qui transforme une phrase en morse en texte

def morse_vers_texte(morse):
    """ Programme qui prend en entrée une liste de code morse et renvoie
        le texte correspondant sous forme de chaine de caractères

        >>> morse_vers_texte(['-.', '...', '..'])
        'NSI'
        >>> morse_vers_texte(['...-', '.-', '-.-.', '.-', '-.', '-.-.', '.', '...'])
        'VACANCES'
        
    """
    texte = ""
    for symbole in morse :
        texte = texte + reverse_dict(alphabet)[symbole]
    return texte

if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose=True)

    texte = input("Quel texte voulez-vous envoyer en morse (sans accent) ?")
    son_morse(texte_vers_morse(texte.upper()))

# fin du fichier code_morse.py
        





            
