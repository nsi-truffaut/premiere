#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : son.py
#  auteur : Grégory Maupu
#    date : 2021/02/18

# Importation des modules

import wave
import pyaudio

def jouer_son(nom_du_fichier):
    # Exemples d'utilisation
    # jouer_son('bip_court.wav')
    
    CHUNK = 1024
    wf = wave.open(nom_du_fichier, 'rb')

    # instantiate PyAudio (1)
    p = pyaudio.PyAudio()

    # open stream (2)
    stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True)

    # read data
    data = wf.readframes(CHUNK)

    # play stream (3)
    while len(data) > 0:
        stream.write(data)
        data = wf.readframes(CHUNK)

    # stop stream (4)
    stream.stop_stream()
    stream.close()

    # close PyAudio (5)
    p.terminate()


# fin du fichier son.py
