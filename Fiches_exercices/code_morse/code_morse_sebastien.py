#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : code_morse_eleve.py
#  auteur : Grégory Maupu
#    date : 2021/02/18

# Importation des modules

#from son import jouer_son
#from time import sleep

# Consigne n°1
# Compléter la fonction ci-dessous :
# Entrées : une chaîne de caractère
# Sorties : la correspondance en code morse de la chaîne entrée

def texte_vers_morse(chaine):
    """ Convertit un texte en morse
    chaine="il fait beau"
    >>> '...-.........-..-..-.....-.....-..-'
    """
    conversion={"a":[".-"],
                "b":["-..."],
                "c":["-.-."],
                "d":["-.."],
                "e":["."],
                "f":["..-."],
                "g":["--."],
                "h":["...."],
                "i":[".."],
                "j":[".---"],
                "k":["-.-"],
                "l":[".-.."],
                "m":["--"],
                "n":["-."],
                "o":["---"],
                "p":[".--."],
                "q":["--.-"],
                "r":[".-."],
                "s":["..."],
                "t":["-"],
                "u":["..-"],
                "v":["...-"],
                "w":[".--"],
                "x":["-..-"],
                "y":["-.--"],
                "z":["--.."],
                " ":["....."]
                }
    a=""
    for i in range(len(chaine)-1):
        if chaine[i]==conversion.keys():
            a=a+conversion.values(i)
        return a

# Consigne n°2 (si nécessaire)
# Compléter la fonction ci-dessous :
# Entrée : un dictionnaire
# Sortie : un dictionnaire dont les clés et valeurs sont le contraire de celui entré

#def reverse_dict(dico):
    #pass


# Consigne n°3
# Compléter la fonction ci-dessous :
# Entrée : une liste de code morse
# Sortie : la chaîne de caractères correspondante

#def morse_vers_texte(code_morse):


# Pour aller plus loin
# Compléter la fonction ci-dessous :
# Entrée : une chaîne de caratères en majuscule
# Sortie : le code morse sous forme de son
#
# Pour ce travail vous avez besoin des deux modules importés en début de fichier
# Il faut décommenter les 2 lignes d'import.
# Vous explorerez le module son pour comprendre comment utiliser la fonction
# Pour la fonction sleep, vous chercherez sa documentation sur le web

#def son_morse(chaine):
    #pass


if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose=True)

    # Les lignes ci-dessous sont à décommenter si vous avez fait la toute fin
    #texte = input("Quel texte voulez-vous envoyer en morse (sans accent) ?")
    #son_morse(texte)

    
# fin du fichier code_morse_eleve.py
