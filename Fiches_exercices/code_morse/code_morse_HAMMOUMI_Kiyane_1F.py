#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : code_morse.py
#  auteur : Grégory Maupu          complété par : HAMMOUMI Kiyane
#    date : 2021/02/18             complété le :  2021/03/09

# Importation des modules

from son import jouer_son
from time import sleep

# Consigne n°1
# Compléter la fonction ci-dessous :
# Entrées : une chaîne de caractère
# Sorties : la correspondance en code morse de la chaîne entrée

morse = {'A':'.-', 'B':'-...', 'C':'-.-.', 'D':'-..', 'E':'.', 'F':'..-.',
         'G':'--.', 'H':'....', 'I':'..', 'J':'.---', 'K':'-.-', 'L':'.-..',
         'M':'--', 'N':'-.', 'O':'---', 'P':'.--.', 'Q':'--.-', 'R':'.-.',
         'S':'...', 'T':'-', 'U':'..-', 'V':'...-', 'W':'.--', 'X':'-..-',
         'Y':'-.--', 'Z':'--..'}

def texte_vers_morse(chaine):
    en_morse=""
    for lettre in chaine :
        if lettre in morse:
            en_morse = en_morse + morse[lettre] + " " 
        else:
            lettre=chr(ord(lettre)-32)
            if lettre in morse:
                en_morse = en_morse + morse[lettre] + " "
            else:
                en_morse= en_morse + "  "
    return en_morse


# Consigne n°2 (si nécessaire)
# Compléter la fonction ci-dessous :
# Entrée : un dictionnaire
# Sortie : un dictionnaire dont les clés et valeurs sont le contraire de celui entré

def reverse_dict(dico):
    old_key=[]
    old_value=[]
    reverse = {}
    roundName=0
    for element in dico:
        old_value.append(dico[element])
        old_key.append(element)
        reverse[old_value[roundName]]=old_key[roundName]
        roundName=roundName + 1
    return reverse


# Consigne n°3
# Compléter la fonction ci-dessous :
# Entrée : une liste de code morse
# Sortie : la chaîne de caractères correspondante

def morse_vers_texte(code_morse):
    dico=reverse_dict(morse)
    en_texte=""
    for code in code_morse :
        if code in dico:
            en_texte = en_texte + dico[code]
        else:
            en_texte= en_texte + " "
    return en_texte


# Pour aller plus loin
# Compléter la fonction ci-dessous :
# Entrée : une chaîne de caratères en majuscule
# Sortie : le code morse sous forme de son
#
# Pour ce travail vous avez besoin des deux modules importés en début de fichier
# Vous explorerez le module son pour comprendre comment utiliser la fonction
# Pour la fonction sleep, vous chercherez sa documentation sur le web

def son_morse(chaine):
    traduction=texte_vers_morse(chaine)
    for bip in traduction:
        if bip == '-':
            jouer_son('bip_long.wav')
            sleep(1)
        elif bip == '.':
            jouer_son('bip_courst.wav')
            sleep(1)
        else:
            sleep(3)
    pass #pas sûr que ça marche puisque le module pyaudio ne fonctionne pas


if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose=True)

    texte = input("Quel texte voulez-vous envoyer en morse (sans accent) ?")
    son_morse(texte)

    
# fin du fichier code_morse_eleve.py
