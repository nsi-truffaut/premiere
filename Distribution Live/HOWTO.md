Auteur : Grégory Maupu
Date : 2021/09/07

# Etape 1 : Installation de Cubic

sudo apt-add-repository ppa:cubic-wizard/release
sudo apt update
sudo apt install --no-install-recommends cubic

# Etape 2 : Télécharger l'ISO Ubuntu Officiel

Se rendre sur https://ubuntu.com/download/desktop

# Etape 3 : Fabrication de son livecd personnalisé

1. Créer un dossier livecd dans Documents
2. Lancer Cubic
3. Sur la première fenêtre, sélectionnez le dossier du projet : livecd.
   Cliquez sur Next
4. Sur la fenêtre suivante, aucune modification n'est nécessaire.
   Cliquez sur Next
5. Patientez jusqu'à voir apparaître un terminal - **Ne pas fermer la fenêtre**

# Etape 4 

1. Ouvrez un Terminal
2. Exécuter le fichier perso.sh

# Etape 5

1. Dans le terminal de l'application Cubic :

   cd home

   ./perso2.sh

2. Patientez jusqu'à ce que l'ensemble des opérations soit terminé.
3. Cliquez sur Next et suivre la procédure jusqu'à l'étape finale.

