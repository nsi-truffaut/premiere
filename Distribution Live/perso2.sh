add-apt-repository main
add-apt-repository universe
add-apt-repository multiverse
add-apt-repository restricted

# Désinstallation de composants inutiles
apt-get -y purge thunderbird gnome-mines gnome-sudoku gnome-mahjongg aisleriot remmina shotwell totem rhythmbox cheese snapd transmission-gtk gnome-calendar ubiquity 

#Installation de logiciels
apt-get -y install linux-generic console-data idle3 thonny jupyter-notebook gimp python3-matplotlib python3-scipy gedit-plugins pdftk dub ocaml-nox vlc git curl ghc apache2 apache2-utils mysql-common mysql-server php7.4 php7.4-mysql php-common php7.4-cli php7.4-common gnome-control-center xournal

#Installation de librairies python
cd ~
pip3 install folium

# Mise à jour

apt-get -y autoremove

#Installation de filius
cd /home
dpkg -i *.deb
rm *.deb
cd ~

#Nettoyage
apt clean

