#Personnalisation

# La future distribution sera dans un dossier livecd de Documents

# Copie de fichiers/dossiers qui seront présents sur la distribution

sudo cp -r perso_config/etc/skel/.* ~/Documents/livecd/custom-root/etc/skel

# Copie des fichiers desktop, nécessaire pour certains logiciels comme logism
sudo cp -r perso_config/usr/share/applications/*.desktop ~/Documents/livecd/custom-root/usr/share/applications

# Copie des applications distribuées en deb

sudo cp perso_config/*.deb ~/Documents/livecd/custom-root/home

# Copie des fichiers nécessaires à l'installation de Scratch 3
sudo cp -r perso_config/scratch ~/Documents/livecd/custom-root/home/scratch

# Copie du fichier sh qui permet d'automatiser la construction de la distribution

sudo cp perso2.sh ~/Documents/livecd/custom-root/home

