# Afficher la première ligne du fichier

with open('disques.csv','r',encoding='utf-8') as fichier :
	ligne_un = fichier.readline()
	print(ligne_un) # Attention, une ligne vide est affichée

# Afficher les deux premières lignes du fichier

with open('disques.csv','r',encoding='utf-8') as fichier :
	ligne_un = fichier.readline()
	ligne_deux = fichier.readline()
	print(ligne_un)
	print(ligne_deux)
	
# Afficher la première ligne sans la ligne vide

with open('disques.csv','r',encoding='utf-8') as fichier :
	ligne_un = fichier.readline().strip()
	print(ligne_un)

# Afficher toutes les lignes du fichier

with open('disques.csv','r',encoding='utf-8') as fichier :
	ligne_lue = fichier.readline().strip()
	while ligne_lue !="" :
		print(ligne_lue)
		ligne_lue = fichier.readline().strip()

# Récupérer la liste des descripteurs

with open('disques.csv','r',encoding='utf-8') as fichier :
	ligne_un = fichier.readline().strip()
	descripteurs = ligne_un.split(',')
	print(descripteurs)

# Afficher seulement les enregistrements du genre "pop"

with open('disques.csv','r',encoding='utf-8') as fichier :
	ligne_lue = fichier.readline().strip()
	descripteurs = ligne_un.split(',')
	while ligne_lue != "" :
		champ_lue = fichier.readline().strip().split(',')
		if len(champ_lue) > 1 and champ_lue[3] =="Pop" :
			print(champ_lue)
			
# On ajoute la condition len(champ_lue) > 1 pour éviter l'erreur de la liste vide.		
		
