#!/usr/bin/env python3

#
# fichier : outils.py (v2)
#  auteur : Pascal CHAUVIN
#    date : 2021-05-23
#


def afficher_table(table, message=""):
	""" Affichage convenable de la table ; la table  est une liste d'enregistrements, 
	    chaque enregistrement est un dictionnaire Python. 
	    
	    Nouvelle version : la largeur de chaque colonne est calculee independamment.
	"""
	
	EXTRA = 1
	
	descripteurs = table[0].keys()
	
	largeurs = dict()
	
	for nom in descripteurs:
		largeurs[nom] = len(str(nom)) + EXTRA
		
	for enregistrement in table:
		for descripteur in descripteurs:
			n = len(str(enregistrement[descripteur])) + EXTRA
			if n > largeurs[descripteur]:
				largeurs[descripteur] = n
	
	formats = dict()
	
	for enregistrement in table:
		for descripteur in descripteurs:
			formats[descripteur] = "|{:<" + str(largeurs[descripteur]) + "}"

	u = ""
	for nom in descripteurs:
		u += formats[nom].format(str(nom))
	u += "|"
	
	print(u) # affiche la ligne des descripteurs (1ere ligne)

	for enregistrement in table:
		u = ""
		for descripteur in descripteurs:
			u += formats[descripteur].format(str(enregistrement[descripteur]))
		u += "|"
		print(u)

	print("--- {}\n".format(message))


def selection(table, criteres):
	""" Selection des lignes selon la valeur logique de "criteres" :
	    
	    Attention !
	    
	    Pour la redaction des criteres (style Python ordinaire pour les tests), 
	    il faudra absolument nommer chaque champ par le mot "enregistrement".
	    
	    Par exemple, pour tester si la valeur du champ "Nom" est "Guido", on ecrira :
	    
	        enregistrement['Nom'] == 'Guido'
	        
	    Les tests sont rediges de la maniere habituelle en Python.
	    """
	
	sel = list()
	for enregistrement in table:
		try:
			if eval(criteres):
				sel.append(enregistrement)
		except:
			print("*** erreur : evaluation impossible ***")
	return sel


def projection(table, liste_descripteurs):
	""" Projection de la table en extrayant les colonnes selon les descripteurs 
	    presents dans la liste "liste_descripteurs" """
	
	proj = list()
	for enregistrement in table:
		d = dict()
		for descripteur in liste_descripteurs:
			if descripteur in enregistrement.keys():
				d[descripteur] = enregistrement[descripteur]
		if d:
			proj.append(d)
	return proj


def tri(table, descripteur, croissant=True):
	""" Tri d'une table selon les valeurs de la colonne "descripteur" """
	
	return sorted(table, key=lambda enregistrement : enregistrement[descripteur], reverse=not croissant)


def Fusion(table1, table2, descripteur1, descripteur2=None):
	""" Fusion de deux tables """
	
	table = list()
	
	if descripteur2 is None:
		descripteur2 = descripteur1
	
	for enregistrement1 in table1:
		for enregistrement2 in table2:
			if enregistrement1[descripteur1] == enregistrement2[descripteur2]:
				enregistrement = dict(enregistrement1)
				for descripteur in enregistrement2:
					if descripteur != descripteur2:
						enregistrement[descripteur] = enregistrement2[descripteur]
				table.append(enregistrement)
	return table


def renommer_descripteur(table, nom1, nom2):
	""" Modifier la table en remplacant l'attribut "nom1" par "nom2" """
	
	if nom1 in table[0].keys():
		for enregistrement in table:
			enregistrement[nom2] = enregistrement[nom1]
			del enregistrement[nom1]


def cloner_table(table):
	""" Cloner une table : la copie est une nouvelle table, independante de l'original en 
	    memoire. """
	
	copie = list()
	
	for enregistrement in table:
		copie_enregistrement = dict(enregistrement)
		copie.append(copie_enregistrement)
	return copie

