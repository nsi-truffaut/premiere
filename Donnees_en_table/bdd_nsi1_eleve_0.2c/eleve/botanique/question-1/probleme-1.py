#!/usr/bin/env python3

#
# fichier : probleme-1.py
#  auteur : Pascal CHAUVIN
#    date : 2021-05-14
#


"""
Question 1 : compter le nombre d'expériences ayant produit des roses pourpres.
"""


import fichiers
import outils


experiences = fichiers.lire_fichier("experiences.csv")
outils.afficher_table(experiences)

pourpres = outils.selection(experiences, "enregistrement['couleur'] == 'pourpre'")
outils.afficher_table(pourpres, "les roses pourpres uniquement !")

fichiers.enregistrer_fichier(pourpres, "pourpres.csv")


print("Nombre d'expériences donnant des roses pourpres :", len(pourpres))

