#!/usr/bin/env python3

#
# fichier : probleme-2.py
#  auteur : Pascal CHAUVIN
#    date : 2021-05-15
#


"""
Question 2 : retrouver les expériences ayant produit des grandes roses à partir du croisement de roses de petites tailles.
"""


from fichiers import *
from outils import *


#
# Importation de la table des expériences en mémoire.
#
experiences = lire_fichier("experiences.csv")
afficher_table(experiences, "Expériences")


#
# Importation de la table des graines en mémoire.
#
graines = lire_fichier("graines.csv")
afficher_table(graines, "Graines")


#
# Sélection des expériences ayant donné de grandes roses seulement.
#
grandes = selection(experiences, "enregistrement['taille'] == 'grande'")
afficher_table(grandes, "Expériences ayant donné de grandes roses")


#
# Projection pour supprimer les colonnes inutiles au calcul.
#
codes_pour_grandes = projection(grandes, ["code_exp", "G1", "G2"])
afficher_table(codes_pour_grandes, "Expériences ayant donné de grandes roses (table simplifiée)")


#
# Jointure pour la première graine.
#
croisements = jointure(codes_pour_grandes, graines, "G1", "code_graine")
renommer_descripteur(croisements, "taille", "taille1")
croisements = projection(croisements, ["code_exp", "G1", "G2", "taille1"])
afficher_table(croisements, "Expériences + 1ère graine")


#
# Jointure pour la seconde graine.
#
croisements = jointure(croisements, graines, "G2", "code_graine")
renommer_descripteur(croisements, "taille", "taille2")
croisements = projection(croisements, ["code_exp", "G1", "taille1", "G2", "taille2"])
afficher_table(croisements, "Expériences + 1ère graine + 2ème graine")


#
# Sélection des expériences qui ne concernent que les petites graines.
#
reponses = selection(croisements, "enregistrement['taille1'] == 'petite' and enregistrement['taille2'] == 'petite'")
afficher_table(reponses, "Expériences + Graines : réponses")


#
# Sauvegarde en fichier de la table des réponses.
#
enregistrer_fichier(reponses, "reponses-2.csv")

