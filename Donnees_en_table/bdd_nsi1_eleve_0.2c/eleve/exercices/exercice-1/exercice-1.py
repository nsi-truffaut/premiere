#!/usr/bin/env python3

#
# fichier : exercice-0.py
#  auteur : Pascal CHAUVIN - Gregory MAUPU
#    date : 2021-05-15
#


"""
Exercice 0
"""

experiences = [
	{
		"code_exp" : 1432,
		"G1" : "C28",
		"G2" : "C29",
		"taille" : "moyenne",
		"couleur" : "blanche"
	}, 
	
	{
		"code_exp" : 1433,
		"G1" : "C28",
		"G2" : "D01",
		"taille" : "grande",
		"couleur" : "pourpre"
	}, 
	
	{
		"code_exp" : 1434,
		"G1" : "D01",
		"G2" : "D03",
		"taille" : "grande",
		"couleur" : "rouge"
	}, 
	
	{
		"code_exp" : 1435,
		"G1" : "D01",
		"G2" : "D32",
		"taille" : "grande",
		"couleur" : "jaune"
	}, 
	
	{
		"code_exp" : 1436,
		"G1" : "B38",
		"G2" : "D01",
		"taille" : "moyenne",
		"couleur" : "jaune"
	}, 
	
	{
		"code_exp" : 1437,
		"G1" : "C29",
		"G2" : "D01",
		"taille" : "moyenne",
		"couleur" : "pourpre"
	}, 
	
	{
		"code_exp" : 1438,
		"G1" : "C29",
		"G2" : "D32",
		"taille" : "petite",
		"couleur" : "pourpre"
	}, 
	
	{
		"code_exp" : 1439,
		"G1" : "C28",
		"G2" : "D32",
		"taille" : "grande",
		"couleur" : "jaune"
	}
	
]


#
# Document a completer au fur et a mesure des questions du sujet
#


