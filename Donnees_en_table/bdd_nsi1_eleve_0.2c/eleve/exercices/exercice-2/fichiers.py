#!/usr/bin/env python3

#
# fichier : fichiers.py
#  auteur : Pascal CHAUVIN
#    date : 2021-05-13
#


def lire_fichier(nom="", sep=";", verbeux=False):
	""" Importation de la table en memoire depuis un fichier de format CSV """
	
	table = []
	
	if verbeux:
		print("Lecture du fichier \"{}\".".format(nom))
	
	with open(nom, "r") as f:

		descripteurs = f.readline().strip().split(sep)
		
		if verbeux:
			print(descripteurs)
			
		ligne = f.readline().strip()
		while ligne != "":
			champs = ligne.split(sep)
			
			if verbeux:
				print(champs)
			
			donnee = {}
			for i in descripteurs:
				donnee[i] = champs[descripteurs.index(i)]
			table.append(donnee)
				
			ligne = f.readline().strip()
		
	return table


def enregistrer_fichier(table=None, nom="", sep=";", fin_ligne='\n', verbeux=False):
	""" Enregistrement de la table en fichier au format CSV """
	
	if verbeux:
		print("Enregistrement du fichier \"{}\".".format(nom))

	if table is None:
		if verbeux:
			print("Erreur : aucune donn\u00E9e.")
	else:
		with open(nom, "w") as f:
			f.write(sep.join(table[0].keys()) + fin_ligne)
			
			for i in range(len(table)):
				f.write(sep.join(table[i].values()) + fin_ligne)

