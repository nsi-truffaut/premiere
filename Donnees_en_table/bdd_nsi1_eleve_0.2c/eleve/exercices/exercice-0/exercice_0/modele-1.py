#!/usr/bin/env python3

#
# fichier : modele-1.py (table vue comme liste de listes)
#  auteur : Pascal CHAUVIN -- Gregory MAUPU
#    date : 2021-05-23
#


with open("graines.csv", "r") as fichier_a_lire:
	
	ligne_lue = fichier_a_lire.readline().strip()
	while ligne_lue != "":
		print(ligne_lue)
		ligne_lue = fichier_a_lire.readline().strip()

