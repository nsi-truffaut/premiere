#!/usr/bin/env python3

#
# fichier : modele-0.py (table vue comme liste de listes)
#  auteur : Pascal CHAUVIN -- Gregory MAUPU
#    date : 2021-05-23
#


fichier_a_lire = open("graines.csv", "r")

ligne_lue = fichier_a_lire.readline().strip()
while ligne_lue != "":
	print(ligne_lue)
	ligne_lue = fichier_a_lire.readline().strip()

fichier_a_lire.close()
