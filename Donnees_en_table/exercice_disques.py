# 1

def est_present(artiste) :
    present = False
    with open("disques.csv",'r') as fichier :
        ligne_lue = fichier.readline()
        while ligne_lue !="" and not present :
            artiste_present = fichier.readline().strip('\n').split(',')[0]
            
            if artiste == artiste_present :
                present = True
            ligne_lue=fichier.readline()
        return present

print(est_present("Jean-Sébastien Blackmore"))
        
# 2

def taille_collection() :
     with open("disques.csv",'r') as fichier :
        ligne_lue = fichier.readline()
        nb_ligne = -1
        while ligne_lue !="" :
            ligne_lue=fichier.readline()
            nb_ligne = nb_ligne+1
        return nb_ligne

print(taille_collection())
            
#3

def nombre_moyen_piste() :
    with open("disques.csv",'r') as fichier :
        ligne_lue = fichier.readline()
        nb_ligne = -1
        m = 0
        while ligne_lue !="" :
            ligne_lue=fichier.readline()
            champ_lue = ligne_lue.strip('\n').split(',')
           
            if len(champ_lue) > 1 :
                m = m + int(champ_lue[4])
            nb_ligne = nb_ligne+1
        return m/nb_ligne

print(nombre_moyen_piste())

#4

def question4():
    with open("disques.csv",'r') as fichier :
        ligne_lue = fichier.readline()
        nom_album = []
        while ligne_lue !="" :
            ligne_lue=fichier.readline()
            champ_lue = ligne_lue.strip('\n').split(',')
            
            if len(champ_lue) > 1 and champ_lue[3] == 'Jazz' and champ_lue[4] == '12' :
                nom_album.append(champ_lue[1])
    return nom_album

print(question4())




            
