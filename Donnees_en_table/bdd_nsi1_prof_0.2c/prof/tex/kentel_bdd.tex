\documentclass[14pt,a4paper,final,french,oneside]{extarticle}

\usepackage{minimal} % version du 2021/05/24

\usepackage{tikz}

\title{Traitements automatisés des données en tables}

\author{Pascal CHAUVIN \and Grégory MAUPU}

\begin{document}


\maketitle

\tableofcontents


%
%
%

\section{Présentation}


\subsection{Botanique}

Une \href{https://player.vimeo.com/video/173747526}{situation} proposée par \ogf{Class'Code\,/\,OpenClassRoom}. 


Nous étudierons dans cette leçon comment un logiciel (et donc des algorithmes) peut traiter automatiquement les données et calculer les réponses aux deux questions posées dans le documentaire.


\newpage


Les informations utiles suivent :


\bigskip
\begin{center}
\includegraphics[scale=0.5]{croisement-2.png}
\end{center}


\bigskip


\subsection{Question 1}

Compter le nombre d'expériences (hybridations génétiques entre variétés de roses) ayant produit des roses pourpres.


\subsection{Question 2}

Retrouver les expériences ayant produit des grandes roses à partir du croisement de roses de petites tailles.


\subsection{Autres aspects (non abordés en 1\up{ère})}

Quelques autres caractéristiques importantes des systèmes de gestion des bases de données:

\begin{itemize}

\item cohérence et intégrité des données;

\item mise à jour et protection des données;

\item gestion des accès contrôlés et/ou concurrents aux données;

\item gestion de données parfois très nombreuses ou volumineuses, pouvant être réparties.

\end{itemize}


%
%
%
\section{Notions et vocabulaire}


Dans le domaine du traitement de données, le modèle de référence, très répandu, est appelé \ogf{modèle relationnel de données} conçu par Edgar~F.~\textsc{Codd}.


Les applications qui mettent en œuvre le modèle de Codd sont appelées SGBDR \ogf{systèmes de gestion des bases de données relationnelles}.

 
L'extraction d'informations nouvelles à partir des informations enregistrées dans une base de donnée est appelée \ogf{requête}: l'utilisateur d'un SGBDR procède par interactions successives avec la base de données, obtenant une réponse à chaque requête.


\subsection{Description des données en tables}


Les données sont disponibles sous différentes formes ou supports matériels :

\begin{itemize}

%\medskip
\item sous forme non numérique (données brutes) : elles sont par exemple issues d'une enquête, sous forme de fiches isolées (ou autres natures) et peut-être avec des informations non uniformisées ;

\medskip
\item sous forme de fichiers numériques enregistrés sur support de stockage : le stockage sous forme numérique impose de définir et respecter un format des informations, en vue de traitements automatisés (programmables) par logiciel, ou encore pour la transmission entre machines (périphériques de stockage ou réseaux) ;

\medskip
\item en mémoire de travail (pour manipulation par un programme) ; les informations sont organisées sous forme de tableaux associatifs (les \emph{dictionnaires} du langage~Python) et de listes.

\end{itemize}

Un format très courant est la table (tableau de lignes et de colonnes), représentée par un fichier de type texte, composé de lignes (une ligne est la collection des informations recueillies pour une donnée). Les lignes sont codées de manière à faire apparaître des colonnes (descripteurs) pour l'utilisateur. Sur une ligne, les différentes informations relatives à une donnée (les colonnes, ou champs) sont séparées par un marqueur (caractère ayant le rôle particulier de découper la ligne, appelé séparateur).

On parle aussi d'\ogf{enregistrement} pour désigner une ligne dans la représentation en table.

Un format très répandu est le format CSV~\ogf{Comma Separated Values}, mais on rencontre aussi le format JSON~\ogf{JavaScript Object Model} (de syntaxe similaire à celle des dictionnaires en Python). Tous deux sont des formats spécialisés de fichiers de type texte.


%
%
%

\section{Les manipulations sur une table}

L'interrogation d'une base de données est nommée \ogf{requête}.

L'exécution d'une requête sur une ou plusieurs tables donne une nouvelle table, résultat de la requête.

Les requêtes sont exprimées à l'aide d'un langage ; SQL \ogf{Structured Query Language} est l'un deux, avec différentes implémentations industrielles parmi lesquelles MySQL, PostgreSQL... Le langage SQL est un standard industriel (qui sera étudié en Terminale~NSI).

\emph{Remarque : pour cette leçon, les scripts utilitaires~\ogf{maison} "fichiers.py" et "outils.py" ont été conçus dans le seul but d'étude (pour présenter les concepts) et ne prétendent pas remplacer un langage comme SQL !}

Un langage d'interrogation de bases de données, organisées selon le modèle de Codd, permet de formuler les requêtes à l'aide d'opérations, dont notamment la~\ogf{sélection}, la~\ogf{projection} et la~\ogf{fusion} .

On distingue les manipulations logiques (A) et les opérations utilitaires (B).


\bigskip
\textbf{Vocabulaire :}

\begin{itemize}

\item table ;

\item descripteur ;

\item enregistrement ;

\item champ ;

\item domaine (de valeur).

\end{itemize}


\bigskip

\begin{center}

\begin{tikzpicture}[scale=0.75]

	\draw[step=0.5,color=gray!25] (0,0) grid (16.5,11);

	\draw[fill=blue!15, opacity=0.5] (1, 9) -- (1, 10) -- (16, 10) -- (16, 9) -- cycle;
	
	\draw[very thick] (1, 2.5) -- (1, 10) -- (16, 10) -- (16, 2.5);

	\draw[very thick, dashed] (1, 1) -- (1, 2.5);

	\draw[very thick, dashed] (16, 1) -- (16, 2.5);

	\draw[very thick] (1, 9) -- (16, 9);

	\draw[very thick] (1, 8) -- (16, 8);

	\draw[very thick] (1, 7) -- (16, 7);

	\draw[very thick] (1, 6) -- (16, 6);

	\draw[very thick] (1, 5) -- (16, 5);

	\draw[very thick] (1, 4) -- (16, 4);

	\draw[very thick] (1, 3) -- (16, 3);


	\draw[very thick, dashed] (4, 1) -- (4, 2.5);

	\draw[very thick] (4, 2.5) -- (4, 10);

	\draw (2.5, 9.5) node {\small \emph{desc. 1}};


	\draw[very thick, dashed] (7, 1) -- (7, 2.5);

	\draw[very thick] (7, 2.5) -- (7, 10);

	\draw (5.5, 9.5) node {\small \emph{desc. 2}};


	\draw[very thick, dashed] (10, 1) -- (10, 2.5);

	\draw[very thick] (10, 2.5) -- (10, 10);

	\draw (8.5, 9.5) node {\small \emph{desc. 3}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

	\draw (11.5, 9.5) node {\small \emph{desc. 4}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

	\draw (14.5, 9.5) node {\small \emph{desc. 5}};

\end{tikzpicture}

\end{center}


\clearpage

\subsection{Projection selon une ou plusieurs colonnes (A)}

Il s'agit ici de ne conserver d'une table que certaines colonnes identifiées par leurs descripteurs.

%\emph{(cf. illustration)}


\bigskip

\begin{center}

\begin{tikzpicture}[scale=0.5]

	\draw[step=0.5,color=gray!25] (0,0) grid (16.5,11);

	\draw[fill=blue!15, opacity=0.5] (1, 9) -- (1, 10) -- (16, 10) -- (16, 9) -- cycle;
	
	\draw[very thick] (1, 2.5) -- (1, 10) -- (16, 10) -- (16, 2.5);

	\draw[very thick, dashed] (1, 1) -- (1, 2.5);

	\draw[very thick, dashed] (16, 1) -- (16, 2.5);

	\draw[very thick] (1, 9) -- (16, 9);

	\draw[very thick] (1, 8) -- (16, 8);

	\draw[very thick] (1, 7) -- (16, 7);

	\draw[very thick] (1, 6) -- (16, 6);

	\draw[very thick] (1, 5) -- (16, 5);

	\draw[very thick] (1, 4) -- (16, 4);

	\draw[very thick] (1, 3) -- (16, 3);


	\draw[very thick, dashed] (4, 1) -- (4, 2.5);

	\draw[very thick] (4, 2.5) -- (4, 10);

%	\draw (2.5, 9.5) node {\scriptsize \emph{desc. 1}};


	\draw[very thick, dashed] (7, 1) -- (7, 2.5);

	\draw[very thick] (7, 2.5) -- (7, 10);

%	\draw (5.5, 9.5) node {\scriptsize \emph{desc. 2}};


	\draw[very thick, dashed] (10, 1) -- (10, 2.5);

	\draw[very thick] (10, 2.5) -- (10, 10);

%	\draw (8.5, 9.5) node {\scriptsize \emph{desc. 3}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

%	\draw (11.5, 9.5) node {\scriptsize \emph{desc. 4}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

%	\draw (14.5, 9.5) node {\scriptsize \emph{desc. 5}};

\end{tikzpicture}

\end{center}


\bigskip

\begin{center}

\begin{tikzpicture}[scale=0.5]

	\draw[step=0.5,color=gray!25] (0,0) grid (16.5,11);

%	\draw[fill=blue!15, opacity=0.5] (1, 9) -- (1, 10) -- (16, 10) -- (16, 9) -- cycle;
%	
%	\draw[very thick] (1, 2.5) -- (1, 10) -- (16, 10) -- (16, 2.5);
%
%	\draw[very thick, dashed] (1, 1) -- (1, 2.5);
%
%	\draw[very thick, dashed] (16, 1) -- (16, 2.5);
%
%	\draw[very thick] (1, 9) -- (16, 9);
%
%	\draw[very thick] (1, 8) -- (16, 8);
%
%	\draw[very thick] (1, 7) -- (16, 7);
%
%	\draw[very thick] (1, 6) -- (16, 6);
%
%	\draw[very thick] (1, 5) -- (16, 5);
%
%	\draw[very thick] (1, 4) -- (16, 4);
%
%	\draw[very thick] (1, 3) -- (16, 3);
%
%
%	\draw[very thick, dashed] (4, 1) -- (4, 2.5);
%
%	\draw[very thick] (4, 2.5) -- (4, 10);
%
%	\draw (2.5, 9.5) node {\scriptsize \emph{desc. 1}};
%
%
%	\draw[very thick, dashed] (7, 1) -- (7, 2.5);
%
%	\draw[very thick] (7, 2.5) -- (7, 10);
%
%	\draw (5.5, 9.5) node {\scriptsize \emph{desc. 2}};
%
%
%	\draw[very thick, dashed] (10, 1) -- (10, 2.5);
%
%	\draw[very thick] (10, 2.5) -- (10, 10);
%
%	\draw (8.5, 9.5) node {\scriptsize \emph{desc. 3}};
%
%
%	\draw[very thick, dashed] (13, 1) -- (13, 2.5);
%
%	\draw[very thick] (13, 2.5) -- (13, 10);
%
%	\draw (11.5, 9.5) node {\scriptsize \emph{desc. 4}};
%
%
%	\draw[very thick, dashed] (13, 1) -- (13, 2.5);
%
%	\draw[very thick] (13, 2.5) -- (13, 10);
%
%	\draw (14.5, 9.5) node {\scriptsize \emph{desc. 5}};

\end{tikzpicture}

\end{center}


\clearpage

\subsection{Sélection selon un critère (A)}

Il s'agit ici de ne conserver d'une table que certaines lignes isolées selon qu'elles remplissent ou non une condition.

%\emph{(cf. illustration)}



\bigskip

\begin{center}

\begin{tikzpicture}[scale=0.5]

	\draw[step=0.5,color=gray!25] (0,0) grid (16.5,11);

	\draw[fill=blue!15, opacity=0.5] (1, 9) -- (1, 10) -- (16, 10) -- (16, 9) -- cycle;
	
	\draw[very thick] (1, 2.5) -- (1, 10) -- (16, 10) -- (16, 2.5);

	\draw[very thick, dashed] (1, 1) -- (1, 2.5);

	\draw[very thick, dashed] (16, 1) -- (16, 2.5);

	\draw[very thick] (1, 9) -- (16, 9);

	\draw[very thick] (1, 8) -- (16, 8);

	\draw[very thick] (1, 7) -- (16, 7);

	\draw[very thick] (1, 6) -- (16, 6);

	\draw[very thick] (1, 5) -- (16, 5);

	\draw[very thick] (1, 4) -- (16, 4);

	\draw[very thick] (1, 3) -- (16, 3);


	\draw[very thick, dashed] (4, 1) -- (4, 2.5);

	\draw[very thick] (4, 2.5) -- (4, 10);

%	\draw (2.5, 9.5) node {\scriptsize \emph{desc. 1}};


	\draw[very thick, dashed] (7, 1) -- (7, 2.5);

	\draw[very thick] (7, 2.5) -- (7, 10);

%	\draw (5.5, 9.5) node {\scriptsize \emph{desc. 2}};


	\draw[very thick, dashed] (10, 1) -- (10, 2.5);

	\draw[very thick] (10, 2.5) -- (10, 10);

%	\draw (8.5, 9.5) node {\scriptsize \emph{desc. 3}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

%	\draw (11.5, 9.5) node {\scriptsize \emph{desc. 4}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

%	\draw (14.5, 9.5) node {\scriptsize \emph{desc. 5}};

\end{tikzpicture}

\end{center}


\bigskip

\begin{center}

\begin{tikzpicture}[scale=0.5]

	\draw[step=0.5,color=gray!25] (0,0) grid (16.5,11);

%	\draw[fill=blue!15, opacity=0.5] (1, 9) -- (1, 10) -- (16, 10) -- (16, 9) -- cycle;
%	
%	\draw[very thick] (1, 2.5) -- (1, 10) -- (16, 10) -- (16, 2.5);
%
%	\draw[very thick, dashed] (1, 1) -- (1, 2.5);
%
%	\draw[very thick, dashed] (16, 1) -- (16, 2.5);
%
%	\draw[very thick] (1, 9) -- (16, 9);
%
%	\draw[very thick] (1, 8) -- (16, 8);
%
%	\draw[very thick] (1, 7) -- (16, 7);
%
%	\draw[very thick] (1, 6) -- (16, 6);
%
%	\draw[very thick] (1, 5) -- (16, 5);
%
%	\draw[very thick] (1, 4) -- (16, 4);
%
%	\draw[very thick] (1, 3) -- (16, 3);
%
%
%	\draw[very thick, dashed] (4, 1) -- (4, 2.5);
%
%	\draw[very thick] (4, 2.5) -- (4, 10);
%
%	\draw (2.5, 9.5) node {\scriptsize \emph{desc. 1}};
%
%
%	\draw[very thick, dashed] (7, 1) -- (7, 2.5);
%
%	\draw[very thick] (7, 2.5) -- (7, 10);
%
%	\draw (5.5, 9.5) node {\scriptsize \emph{desc. 2}};
%
%
%	\draw[very thick, dashed] (10, 1) -- (10, 2.5);
%
%	\draw[very thick] (10, 2.5) -- (10, 10);
%
%	\draw (8.5, 9.5) node {\scriptsize \emph{desc. 3}};
%
%
%	\draw[very thick, dashed] (13, 1) -- (13, 2.5);
%
%	\draw[very thick] (13, 2.5) -- (13, 10);
%
%	\draw (11.5, 9.5) node {\scriptsize \emph{desc. 4}};
%
%
%	\draw[very thick, dashed] (13, 1) -- (13, 2.5);
%
%	\draw[very thick] (13, 2.5) -- (13, 10);
%
%	\draw (14.5, 9.5) node {\scriptsize \emph{desc. 5}};

\end{tikzpicture}

\end{center}


\clearpage

\subsection{Fusion de deux tables (A)}

Il s'agit ici de créer une nouvelle table, à partir de deux tables existantes : l'association de deux lignes issues des deux tables pourra être rendue possible, par exemple, par le fait qu'elles présentent, aux colonnes de même signification, des valeurs identiques.

%\emph{(cf. illustration)}


\bigskip

\begin{tabular}{cc}

\begin{tikzpicture}[scale=0.5]

	\draw[step=0.5,color=gray!25] (0,0) grid (16.5,11);

	\draw[fill=green!15, opacity=0.5] (1, 9) -- (1, 10) -- (16, 10) -- (16, 9) -- cycle;
	
	\draw[very thick] (1, 2.5) -- (1, 10) -- (16, 10) -- (16, 2.5);

	\draw[very thick, dashed] (1, 1) -- (1, 2.5);

	\draw[very thick, dashed] (16, 1) -- (16, 2.5);

	\draw[very thick] (1, 9) -- (16, 9);

	\draw[very thick] (1, 8) -- (16, 8);

	\draw[very thick] (1, 7) -- (16, 7);

	\draw[very thick] (1, 6) -- (16, 6);

	\draw[very thick] (1, 5) -- (16, 5);

	\draw[very thick] (1, 4) -- (16, 4);

	\draw[very thick] (1, 3) -- (16, 3);


	\draw[very thick, dashed] (4, 1) -- (4, 2.5);

	\draw[very thick] (4, 2.5) -- (4, 10);

%	\draw (2.5, 9.5) node {\scriptsize \emph{desc. 1}};


	\draw[very thick, dashed] (7, 1) -- (7, 2.5);

	\draw[very thick] (7, 2.5) -- (7, 10);

%	\draw (5.5, 9.5) node {\scriptsize \emph{desc. 2}};


	\draw[very thick, dashed] (10, 1) -- (10, 2.5);

	\draw[very thick] (10, 2.5) -- (10, 10);

%	\draw (8.5, 9.5) node {\scriptsize \emph{desc. 3}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

%	\draw (11.5, 9.5) node {\scriptsize \emph{desc. 4}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

%	\draw (14.5, 9.5) node {\scriptsize \emph{desc. 5}};

\end{tikzpicture}

%\end{center}

& 

%\bigskip

%\begin{center}

\begin{tikzpicture}[scale=0.5]

	\draw[step=0.5,color=gray!25] (0,0) grid (16.5,11);

	\draw[fill=red!15, opacity=0.5] (1, 9) -- (1, 10) -- (16, 10) -- (16, 9) -- cycle;
	
	\draw[very thick] (1, 2.5) -- (1, 10) -- (16, 10) -- (16, 2.5);

	\draw[very thick, dashed] (1, 1) -- (1, 2.5);

	\draw[very thick, dashed] (16, 1) -- (16, 2.5);

	\draw[very thick] (1, 9) -- (16, 9);

	\draw[very thick] (1, 8) -- (16, 8);

	\draw[very thick] (1, 7) -- (16, 7);

	\draw[very thick] (1, 6) -- (16, 6);

	\draw[very thick] (1, 5) -- (16, 5);

	\draw[very thick] (1, 4) -- (16, 4);

	\draw[very thick] (1, 3) -- (16, 3);


	\draw[very thick, dashed] (4, 1) -- (4, 2.5);

	\draw[very thick] (4, 2.5) -- (4, 10);

%	\draw (2.5, 9.5) node {\scriptsize \emph{desc. 1}};


	\draw[very thick, dashed] (7, 1) -- (7, 2.5);

	\draw[very thick] (7, 2.5) -- (7, 10);

%	\draw (5.5, 9.5) node {\scriptsize \emph{desc. 2}};


	\draw[very thick, dashed] (10, 1) -- (10, 2.5);

	\draw[very thick] (10, 2.5) -- (10, 10);

%	\draw (8.5, 9.5) node {\scriptsize \emph{desc. 3}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

%	\draw (11.5, 9.5) node {\scriptsize \emph{desc. 4}};


	\draw[very thick, dashed] (13, 1) -- (13, 2.5);

	\draw[very thick] (13, 2.5) -- (13, 10);

%	\draw (14.5, 9.5) node {\scriptsize \emph{desc. 5}};

\end{tikzpicture}

%\end{center}

\end{tabular}


\bigskip

\begin{center}

\begin{tikzpicture}[scale=0.5]

	\draw[step=0.5,color=gray!25] (0,0) grid (16.5,11);

%	\draw[fill=blue!15, opacity=0.5] (1, 9) -- (1, 10) -- (16, 10) -- (16, 9) -- cycle;
%	
%	\draw[very thick] (1, 2.5) -- (1, 10) -- (16, 10) -- (16, 2.5);
%
%	\draw[very thick, dashed] (1, 1) -- (1, 2.5);
%
%	\draw[very thick, dashed] (16, 1) -- (16, 2.5);
%
%	\draw[very thick] (1, 9) -- (16, 9);
%
%	\draw[very thick] (1, 8) -- (16, 8);
%
%	\draw[very thick] (1, 7) -- (16, 7);
%
%	\draw[very thick] (1, 6) -- (16, 6);
%
%	\draw[very thick] (1, 5) -- (16, 5);
%
%	\draw[very thick] (1, 4) -- (16, 4);
%
%	\draw[very thick] (1, 3) -- (16, 3);
%
%
%	\draw[very thick, dashed] (4, 1) -- (4, 2.5);
%
%	\draw[very thick] (4, 2.5) -- (4, 10);
%
%	\draw (2.5, 9.5) node {\scriptsize \emph{desc. 1}};
%
%
%	\draw[very thick, dashed] (7, 1) -- (7, 2.5);
%
%	\draw[very thick] (7, 2.5) -- (7, 10);
%
%	\draw (5.5, 9.5) node {\scriptsize \emph{desc. 2}};
%
%
%	\draw[very thick, dashed] (10, 1) -- (10, 2.5);
%
%	\draw[very thick] (10, 2.5) -- (10, 10);
%
%	\draw (8.5, 9.5) node {\scriptsize \emph{desc. 3}};
%
%
%	\draw[very thick, dashed] (13, 1) -- (13, 2.5);
%
%	\draw[very thick] (13, 2.5) -- (13, 10);
%
%	\draw (11.5, 9.5) node {\scriptsize \emph{desc. 4}};
%
%
%	\draw[very thick, dashed] (13, 1) -- (13, 2.5);
%
%	\draw[very thick] (13, 2.5) -- (13, 10);
%
%	\draw (14.5, 9.5) node {\scriptsize \emph{desc. 5}};

\end{tikzpicture}

\end{center}


%\subsection{Intersection (A) (+)}
%
%
%\subsection{Union (A) (+)}
%
%
%\subsection{Différence (A) (+)}
%
%
%\subsection{Produit cartésien (A) (+)}


\subsection{Quelques opérations utilitaires (B)}


Pour l'enchaînement des requêtes, les opérations suivantes peuvent être employées:

\begin{itemize}

\item duplication (clonage) d'une table ;

\item renommage d'un descripteur dans une table.

\end{itemize}


\newpage


%
%
%
\section{Exercices}


\subsection{Exercice 0 : lecture et écriture de fichiers}


On expose ici le principe d'accès aux \emph{fichiers de type~texte}, en lecture ou en écriture.

Un fichier texte (désigné par son nom dans le système d'exploitation) est généralement lu ou écrit, ligne après ligne, grâce au(x) caractère(s) invisible(s) de fin de ligne.

L'algorithme suivant présente la méthode usuelle pour travailler sur un fichier texte :

\begin{algorithm}

%\KwData{
%
%\qquad t est un tableau de nombres
%
%}

%\KwResult{
%
%\qquad unique est le seul élément de t distinct de tous les autres
%
%}

\SetAlgoLined

\Begin{

ouvrir l'accès au fichier (en écriture ou en lecture)

lire la première ligne

\While{la ligne lue n'est pas une chaîne vide}{

	traiter cette ligne
	
	lire la ligne suivante
	
}

fermer l'accès au fichier

}

\end{algorithm}

En langage~Python~3, on dispose de deux modèles pour traduire l'algorithme précédent. Les deux modèles réalisent la même tâche, mais avec quelques différences dans l'écriture.

\medskip
On observera et retiendra que l'argument "r" signifie une ouverture en lecture, alors que l'argument "w" signifie une ouverture en écriture.

\subsubsection{Modèle traditionnel}

En guise d'exemple, le fichier "graines.csv" sera lu ligne après ligne depuis le répertoire courant (celui contenant le script~"modele-0.py") et chaque ligne sera affichée sur la console.

\medskip
\begin{Python3}[modele-0.py]

fichier_a_lire = open("graines.csv", "r")

ligne_lue = fichier_a_lire.readline().strip()
while ligne_lue != "":
	print(ligne_lue) # ou tout autre traitement
	ligne_lue = fichier_a_lire.readline().strip()

fichier_a_lire.close()

\end{Python3}

La fonction~\japy{strip()} est une méthode du type chaîne de caractère en Python (type \japy{string}), qui élimine les espaces visibles ou non, en début et fin de chaîne.


\clearpage


\subsubsection{Nouveau modèle (depuis Python~3)}

C'est le modèle à favoriser depuis les versions 3 et suivantes du langage Python.

\medskip
\begin{Python3}[modele-1.py]

with open("graines.csv", "r") as fichier_a_lire:

	ligne_lue = fichier_a_lire.readline().strip()
	while ligne_lue != "":
		print(ligne_lue) # ou tout autre traitement
		ligne_lue = fichier_a_lire.readline().strip()

\end{Python3}


Cet exercice illustre les techniques utilisées pour la suite de la leçon.


\begin{enumerate}

\item \'{E}crire un script~"fic1.py" qui lira le fichier "graines.csv" du répertoire courant, et affichera chaque ligne précédée du signe~"+" et terminée par le signe~"-".

En fin d'exécution, le nombre de lignes affichées sera indiqué à l'écran.

\item \'{E}crire un script~"fic2.py" qui lira le fichier "graines.csv" du répertoire courant, qui enregistrera chaque ligne précédée du signe~"+" et terminée par le signe~"-" dans un nouveau fichier texte nommé "graines.txt". 

\emph{Vous rechercherez dans la documentation officielle du langage~Python la fonction qui permet l'écriture demandée.}

\end{enumerate}


\subsection{Exercice 1 : botanique (suite)}


On reprend les expériences de botanique.

Il s'agit de répondre par programme, et uniquement à l'aide des fonctions de la librairie standard de Python, aux questions suivantes.

\emph{Il est donc exclu, pour l'exercice, de faire appel aux scripts "fichiers.py" et "outils.py" : on utilisera uniquement les fonctions et structures de données du langage Python~3.}

\begin{enumerate}

\item Quel est le nombre d'expériences consignées dans la table~\ogf{experiences} ?

\item Donner toutes les informations relatives à la quatrième expérience.

\item Donner la nature du descripteur~\ogf{taille}.

\item Donner le domaine du champ~\ogf{couleur}, c'est-à-dire la liste de toutes les valeurs possibles du champ~\ogf{couleur}.

\item Déterminer les codes de toutes les expériences qui ont fait intervenir la rose~\ogf{C29}.

\item \begin{enumerate}

\item En proposant une adaptation du programme de \emph{tri par sélection} (étudié en classe de façon générale sur un tableau), obtenir un classement des expériences par ordre croissant des codes.

\item Cette même adaptation convient-elle pour un classement selon les tailles de graine ?

\end{enumerate}

\end{enumerate}



\subsection{Exercice 2 : moyennes trimestrielles}


\begin{enumerate}

\item Obtenir la liste des élèves.

\item Obtenir la liste des élèves ayant une moyenne au moins égale à 10 en Espagnol.

\item Calculer la moyenne des élèves qui ont obtenu au moins la moyenne 10 en Espagnol.

\end{enumerate}


\subsection{Exercice 3 : recettes de cuisine préférées}


Pour mieux recevoir ses ami.e.s à manger, Bob avait jusqu'à ce jour consigné dans un tableau les goûts de ses amis : les colonnes (sauf la première) indiquent un plat, et les lignes indiquent le prénom puis les goûts d'un.e ami.e. Pour simplifier l'exercice, tous les individus portent des prénoms distincts.

Mais le système se montre peu pratique à l'emploi et Alice (qui partage les mêmes ami.e.s) décide de réorganiser les données de Bob : elle souhaite remplacer ce grand tableau par une fiche pour chaque invité.e.

Alice décide donc de construire pour chaque connaissance une table, dont les descripteurs seront "prenom" (chaîne de caractères) et "aime\_bien" (booléen), et dans laquelle chaque enregistrement référence un plat et le goût de l'ami pour ce plat (vrai ou faux à la proposition "aime\_bien").

Alice souhaite écrire un programme Python qui générera automatiquement toutes les tables, ami par ami, à partir du tableau initial. 

\begin{enumerate}

\item Créer en mémoire la table "table\_recettes" à partir du fichier "recettes.csv".

\item En déduire une variable "liste\_amis" (de type liste) qui conserve les prénoms de tous les amis.

\item Créer une variable "liste\_plats" (de type liste) qui conserve les noms des plats.

\item Construire une table des plats que Lucie aime bien, puis en déduire la liste des plats que Lucie aime bien.

\item Construire (et enregistrer sous forme de fichier au format~CSV) une table des plats pour Lucie, mentionnant tous les plats, aimés ou non par Lucie.

\item Pour achever le projet d'Alice, créer automatiquement (et sauvegarder) l'équivalent de la liste des plats pour Lucie, cette fois pour chacun.e des ami.e.s.


\end{enumerate}


\clearpage


\subsection{Exercice 4 : données géographiques pour la France}


\emph{Attention ! Les données manipulées sont très volumineuses : il est conseillé de bien vérifier chaque programme avant son exécution.}

\begin{enumerate}

\item Obtenir les nom, latitude et longitude de chaque lieu enregistré dans la base de données.

\item Obtenir les informations concernant Challans et Nantes.

\item Déterminer le nom et le nombre d'habitants des lieux d'au moins \num{100000}~habitants.

\item Déterminer le nom des villes d'au moins \num{100000}~habitants.

\end{enumerate}


%\subsection{Collection de disques}
%
%
%\subsection{Recettes de cuisines}
%
%
%\subsection{Femmes scientifiques}
%
%
%\subsection{Prises de commandes dans une pizzeria}


%
%
%
\section{Sources documentaires}


\begin{itemize}

\medskip
\item Données géographiques issues du site \href{https://www.geonames.org}{GeoNames} (donnée ouverte) ;

\medskip
\item Class'Code\,/\,OpenClassRoom ;

\medskip
\item Collection Prépabac NSI~1\up{ère} -- \'{E}ditions Hatier ;

\medskip
\item Revue Tangente--\'{E}ducation n\up{o} 42--43 \ogf{L'informatique débranchée}.

\end{itemize}


\end{document}
