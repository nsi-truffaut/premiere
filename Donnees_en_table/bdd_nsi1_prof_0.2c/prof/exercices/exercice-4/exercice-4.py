#!/usr/bin/env python3

#
# fichier : exercice-3.py
#  auteur : Pascal CHAUVIN - Gregory MAUPU
#    date : 2021-05-15
#


"""
Exercice 3 : 
"""

from fichiers import *
from outils import *


#
# Importation du fichier des informations geographiques.
#
# Source : http://www.geonames.org/
#
lieux = lire_fichier("FR.csv")


#
# Attention !!!
# 
# Le fichier "FR.csv" est tres volumineux (> 21 Mo de texte pur). Il faut par consequent 
# choisir avec precaution les differentes qui seront realisees pour ne pas saturer 
# la memoire.
#


#
# Projection : obtenir la liste des nom, latitude, longitude.
#
t = projection(lieux, ["name", "latitude", "longitude", "population"])


#
# Sélection : obtenir les informations de Nantes et Challans.
#
u = selection(t, "enregistrement['name'] in ['Challans', 'Nantes']")
v = tri(u, "name", croissant=True)
afficher_table(v)

enregistrer_fichier(v, "challans_nantes.csv")


#
# Obtenir les informations des villes d'au moins 100000 habitants.
#
t = projection(lieux, ["name", "population"])
u = selection(t, "int(enregistrement['population']) >= 100000")
v = tri(u, "name", croissant=True)
afficher_table(v)


#
# Obtenir les informations des arrondissements de moins de 150000 habitants.
#
t = projection(lieux, ["name", "population"])
u = selection(t, "('Arrondissement' in str(enregistrement['name'])) and (int(enregistrement['population']) < 150000)")
v = tri(u, "name", croissant=True)
afficher_table(v, "arrondissements de moins de 150000 hab.")

enregistrer_fichier(v, "grandes_villes.csv")
