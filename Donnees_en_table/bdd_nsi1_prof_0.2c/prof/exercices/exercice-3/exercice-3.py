#!/usr/bin/env python3

#
# fichier : exercice-2.py
#  auteur : Pascal CHAUVIN - Gregory MAUPU
#    date : 2021-05-13
#


"""
Exercice : recettes de cuisine et amis
"""


from fichiers import *
from outils import *


#
# Importation du fichier des recettes et amis.
#

table_recettes = lire_fichier("recettes.csv")
#afficher_table(table_recettes, "Recettes préférées des amis")


#
# Obtenir la liste des amis.
#

table_amis = projection(table_recettes, ["prenom"])
table_amis = tri(table_amis, "prenom", croissant=True)
afficher_table(table_amis, "Amis en ordre alphabétique")

liste_amis = [a['prenom'] for a in table_amis]

print(liste_amis)
print("---")


#
# Obtenir la liste des plats
#

liste_plats = sorted([p for p in table_recettes[0].keys() if p != "prenom"])

print(liste_plats)
print("---")


#
# Obtenir la liste des plats que Lucie aime bien.
#
table_plats_Lucie = selection(table_recettes, "enregistrement['prenom'] == 'Lucie'")

sel = []

for p in liste_plats:
	t = projection(table_plats_Lucie, [p])
	s = selection(t, "enregistrement['" + p + "'] == '*'")
	if len(s) == 1:
		sel.append(p)

print("Lucie :", sel)
print("---")


#
# Creation de table des plats de Lucie
#

table_Lucie = []

for p in liste_plats:
	plat_Lucie = {}
	plat_Lucie['plat'] = p
	plat_Lucie['aime_bien'] = str(p in sel)
	table_Lucie.append(plat_Lucie)

afficher_table(table_Lucie, "Plats de Lucie")


#
# Creation des tables des plats de tous les amis
#

for ami in liste_amis:
	table_ami = []

	table_plats_ami = selection(table_recettes, "enregistrement['prenom'] == '" + ami + "'")

	sel_ami = []

	for p in liste_plats:
		t = projection(table_plats_ami, [p])
		s = selection(t, "enregistrement['" + p + "'] == '*'")
		if len(s) == 1:
			sel_ami.append(p)

	for p in liste_plats:
		plat = {}
		plat['plat'] = p
		plat['aime_bien'] = str(p in sel_ami)
		table_ami.append(plat)
	
	message = "Plats de " + ami
	enregistrer_fichier(table_ami, "plats_" + ami + ".csv")

