#!/usr/bin/env python3

#
# fichier : exercice-1.py
#  auteur : Pascal CHAUVIN - Gregory MAUPU
#    date : 2021-05-13
#


"""
Exercice : 
"""

import fichiers
import outils


#
# Importation du fichier des élèves.
#
les_moyennes = fichiers.lire_fichier("moyennes.csv")
outils.afficher_table(les_moyennes, "Moyennes du trimestre")


#
# Projection : obtenir la liste des élèves.
#
t = outils.projection(les_moyennes, ["Nom"])
tri1 = outils.tri(t, "Nom", croissant=True)
outils.afficher_table(tri1, "Liste des élèves (ordre alphabétique)")


#
# Projection : obtenir la liste des élèves ayant une moyenne d'au moins 10 en Espagnol.
#
notes_espagnol = outils.projection(les_moyennes, ["Nom", "Espagnol"])
notes_esp_sup_moy = outils.selection(notes_espagnol, "float(enregistrement['Espagnol']) >= 10")
reponse_triee = outils.tri(notes_esp_sup_moy, "Nom", croissant=True)
outils.afficher_table(reponse_triee, "Liste des élèves (ordre alphabétique)")


#
# Calculer la moyenne des élèves ayant au moins la moyenne de 10 en Espagnol.
#
somme = 0
for moyenne in reponse_triee:
	somme += float(moyenne['Espagnol'])
print(somme/len(reponse_triee))

