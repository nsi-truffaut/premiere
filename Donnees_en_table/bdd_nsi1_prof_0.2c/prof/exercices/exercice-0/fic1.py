#!/usr/bin/env python3

#
# fichier : fic1.py
#  auteur : Pascal CHAUVIN -- Gregory MAUPU
#    date : 2021-05-24
#

nombre_de_lignes = 0 # lignes non vides

with open("graines.csv", "r") as fichier_a_lire:
	
	ligne_lue = fichier_a_lire.readline().strip()
	while ligne_lue != "":

		nombre_de_lignes += 1
		
		ligne_lue = "+" + ligne_lue + "-"

		print(ligne_lue)
		ligne_lue = fichier_a_lire.readline().strip()

print(nombre_de_lignes, "lignes non vides lues")
