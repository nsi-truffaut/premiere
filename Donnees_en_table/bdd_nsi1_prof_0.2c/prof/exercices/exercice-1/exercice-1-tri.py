#!/usr/bin/env python3

#
# fichier : exercice-0-tri.py (table vue comme liste de listes)
#  auteur : Pascal CHAUVIN -- Gregory MAUPU
#    date : 2021-05-18
#


def afficher_table(t):
	""" affichage de la table (liste de listes) """
	for enregistrement in t:
		for champ in enregistrement:
			print(champ, end="| ")
		print()
	print("---")


#
# Lecture du fichier "experiences.csv" et constitution de la table "la_table"
#
la_table = []

with open("experiences.csv", "r") as fichier_a_lire:
	
	descripteurs = fichier_a_lire.readline().strip().split(";")
	
	la_table.append(descripteurs)
	
	ligne = fichier_a_lire.readline().strip()
	while ligne != "":
		champs = ligne.split(";")
		
		donnees = []
		for i in descripteurs:
			donnees.append(champs[descripteurs.index(i)])
		la_table.append(donnees)
				
		ligne = fichier_a_lire.readline().strip()


afficher_table(la_table)


#
# Question : quelles sont les modifications par rapport à l'algorithme "standard" ?
#
def tri_selection(t, indice_colonne):
	for i in range(1, len(t)-1):
		j_min = i
		for j in range(i+1, len(t)):
			if t[j][indice_colonne] < t[j_min][indice_colonne]:
				j_min = j
		if j_min != i:
			t[i], t[j_min] = t[j_min], t[i]


#
# Dans l'exemple ci-dessous, on trie selon le descripteur d'indice 3 dans la liste
# "descripteurs" (i.e. le 4ieme descripteur, correspondant au descripteur "taille").
#
# Remarque : on peut bien sur remplacer le numero (indice_colonne) par la chaine 
# correspondante (ici 3 est l'indice du descripteur "taille" dans la liste 
# des descripteurs), mais de fait, cela exige de rechercher l'indice du 
# descripteur dans la liste des descripteurs, et donc un peu de code supplementaire.
#
tri_selection(la_table, 3)


afficher_table(la_table)

