#!/usr/bin/env python3

#
# fichier : exercice-0.py
#  auteur : Pascal CHAUVIN - Gregory MAUPU
#    date : 2021-05-15
#


"""
Exercice 0 : 
"""

experiences = [
	{
		"code_exp" : 1432,
		"G1" : "C28",
		"G2" : "C29",
		"taille" : "moyenne",
		"couleur" : "blanche"
	}, 
	
	{
		"code_exp" : 1433,
		"G1" : "C28",
		"G2" : "D01",
		"taille" : "grande",
		"couleur" : "pourpre"
	}, 
	
	{
		"code_exp" : 1434,
		"G1" : "D01",
		"G2" : "D03",
		"taille" : "grande",
		"couleur" : "rouge"
	}, 
	
	{
		"code_exp" : 1435,
		"G1" : "D01",
		"G2" : "D32",
		"taille" : "grande",
		"couleur" : "jaune"
	}, 
	
	{
		"code_exp" : 1436,
		"G1" : "B38",
		"G2" : "D01",
		"taille" : "moyenne",
		"couleur" : "jaune"
	}, 
	
	{
		"code_exp" : 1437,
		"G1" : "C29",
		"G2" : "D01",
		"taille" : "moyenne",
		"couleur" : "pourpre"
	}, 
	
	{
		"code_exp" : 1438,
		"G1" : "C29",
		"G2" : "D32",
		"taille" : "petite",
		"couleur" : "pourpre"
	}, 
	
	{
		"code_exp" : 1439,
		"G1" : "C28",
		"G2" : "D32",
		"taille" : "grande",
		"couleur" : "jaune"
	}
	
]


#
# Quel est le nombre d'expériences consignées dans la table ?
#
print(len(experiences))


#
# Donner toutes les informations relatives à la quatrième expérience.
#
x = experiences[3]

for clef in x.keys():
	print(clef, x[clef])


#
# Donner la nature du descripteur "taille".
#
print(type(x["taille"]))


#
# Donner le domaine du champ "couleur", c'est-à-dire la liste de 
# toutes les valeurs possibles du champ "couleur".
#
domaine_couleurs = []

for e in experiences:
	if e["couleur"] not in domaine_couleurs:
		domaine_couleurs.append(e["couleur"])

print(domaine_couleurs)


#
# Déterminer les codes des expériences qui ont fait intervenir 
# la rose "C29".
#
exp_avec_C29 = []

for e in experiences:
	if (e["G1"] == "C29") or (e["G2"] == "C29"):
		if (not e["code_exp"] in exp_avec_C29):
			exp_avec_C29.append(e["code_exp"])

print(exp_avec_C29)

