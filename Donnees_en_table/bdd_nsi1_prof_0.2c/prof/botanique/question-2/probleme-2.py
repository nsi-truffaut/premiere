#!/usr/bin/env python3

#
# fichier : probleme-2.py
#  auteur : Pascal CHAUVIN
#    date : 2021-05-24
#


"""
Question 2 : retrouver les expériences ayant produit des grandes roses à partir du croisement de roses de petites tailles.
"""


from fichiers import *
from outils import *


#
# Importation de la table des expériences en mémoire.
#
experiences = lire_fichier("experiences.csv")
afficher_table(experiences, "Expériences")


#
# Importation de la table des graines en mémoire.
#
graines = lire_fichier("graines.csv")
afficher_table(graines, "Graines")


#
# Sélection des expériences ayant donné de grandes roses seulement.
#
grandes = selection(experiences, "enregistrement['taille'] == 'grande'")
afficher_table(grandes, "Expériences ayant donné de grandes roses")


#
# Projection pour supprimer les colonnes inutiles au calcul.
#
codes_pour_grandes = projection(grandes, ["code_exp", "G1", "G2"])
afficher_table(codes_pour_grandes, "Expériences ayant donné de grandes roses (table simplifiée)")


#
# Clonage de la table des graines, en renommant les descripteurs de la copie graines1,
# (pour permettre la fusion).
#
graines1 = cloner_table(graines)
renommer_descripteur(graines1, "code_graine", "G1")
renommer_descripteur(graines1, "taille", "taille1")
graines1 = projection(graines1, ["G1", "taille1"])
afficher_table(graines1, "Graines1")


#
# Fusion pour la première graine.
#
croisements = fusion(codes_pour_grandes, graines1, "G1", "G1")
afficher_table(croisements, "Expériences + 1ère graine")


#
# Clonage de la table des graines, en renommant les descripteurs de graines2.
#
graines2 = cloner_table(graines)
renommer_descripteur(graines2, "code_graine", "G2")
renommer_descripteur(graines2, "taille", "taille2")
graines2 = projection(graines2, ["G2", "taille2"])
afficher_table(graines2, "Graines2")


#
# Fusion pour la seconde graine.
#
croisements = fusion(croisements, graines2, "G2", "G2")
afficher_table(croisements, "Expériences + 1ère graine + 2ème graine")


#
# Sélection des expériences qui ne concernent que les petites graines.
#
reponses = selection(croisements, "enregistrement['taille1'] == 'petite' and enregistrement['taille2'] == 'petite'")
afficher_table(reponses, "Expériences + Graines : réponses")


#
# Sauvegarde en fichier de la table des réponses.
#
enregistrer_fichier(reponses, "reponses-2.csv")

