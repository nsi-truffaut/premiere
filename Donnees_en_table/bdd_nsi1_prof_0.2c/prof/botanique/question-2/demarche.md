# Démarche (méthode 1)

On travaille à partir des informations de deux tables : "Expériences" et "Graines".

Dans la table "experiences", on sélectionne tous les enregistrements dont le descripteur "taille" possède la valeur "grande" : on obtient la table "grandes".

Du résultat précédent, on écarte les informations inutiles et ne conserve que les informations pertinentes : on projette selon les descripteurs "code_exp", "G1" et "G2".

On clone la table des graines en une table "graines1", en renommant le descripteur "code_graine" en "G1", et le descripteur "taille" en "taille1" : on obtient une table "graines1" décrivant la "première" graine. On simplifie, par projection, la table "graines1" pour ne conserver que les champs pertinents "G1" et "taille1".

On peut alors effectuer une première jointure entre la table des expériences et la table "graines1".

On clone à nouveau la table des graines en une table "graines2", en effectuant les mêmes renommages : on obtient une table "graines2" décrivant la "seconde" graine.

On effectue une seconde jointure, à partir de la première jointure et de la table "graines2".

Dans la seconde jointure, il reste à sélectionner les enregistrements pour lesquels les descripteurs "taille1" et "taille2" ont pris la valeur "petite".
