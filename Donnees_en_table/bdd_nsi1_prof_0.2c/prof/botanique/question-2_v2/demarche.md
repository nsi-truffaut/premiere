# Démarche (méthode 2)

On va cette fois présenter une méthode plus directe, en évitant les clonages de tables.

On procède de la même manière jusqu'à obtenir une tables "experiences" simplifiée.

On pratique immédiatement une première jointure entre la table des expériences et la table "graines" : la table "croisements" qui en résulte possède désormais un descripteur "taille" qui est devenu ambigü. On renomme donc ce descripteur en "taille1", puis on a effectué sur la "première" graine.

On pratiqueensuite une seconde jointure entre la nouvelle table des expériences et la table "graines" : la table "croisements" qui en résulte possède désormais un nouveau descripteur "taille" lui aussi devenu ambigü. On renomme donc ce descripteur en "taille2".

Comme en première méthode, on termine en sélectionnant, dans la seconde jointure, les enregistrements pour lesquels les descripteurs "taille1" et "taille2" ont pris la valeur "petite".
