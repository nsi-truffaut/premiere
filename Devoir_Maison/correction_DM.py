BDPrix = {'Sabre laser' : 229 , 'Etoile de ninja' : 29.95 , 'Cape' : 75, 'Baguette' : 35, 'Chapeau' : 12, 'Bandeau' : 12, 'Balai' : 130}

a= []
b = []
for g in BDPrix :
    a.append(g)
print(a)
for k in range(len(BDPrix)) :
    if 5 <= BDPrix[a[k]] <= 100 :
        b.append(a[k])
print(b)
#Solution 1

def Dispo(p,D) :
    if p in D.keys():
        return True
    else :
        return False

#Solution 2

def Dispo2(p,D) :
    return p in D.keys()

def PrixMoyen(D) :
    s = 0
    for prix in D.values() :
        s = s + prix
    return s/len(D.keys())

def IntervallePrix(m,M,D):
    liste_produit = []
    for produit in D.keys() :
        if D[produit] > m and D[produit] < M :
            liste_produit.append(produit)
    return liste_produit

#Test

print(Dispo('Cape',BDPrix))
print(Dispo('Voiture',BDPrix))

print(PrixMoyen(BDPrix))

print(IntervallePrix(25,50,BDPrix))

panier = {'Sabre laser' : 2,'Etoile de ninja': 3, 'Bandeau':3}
panier2 = {'Sabre laser' : 2,'voiture': 3, 'Bandeau':3}

def TousDispo(D1,D2) :
    dispo = True
    k = 0
    for achat in D2.keys() :
        if achat not in D1.keys() :
            return not dispo
    return dispo

def Total(D1,D2) :
    facture = 0
    for achat in D2.keys():
        facture = facture + D2[achat]*D1[achat]
    return facture

#Test

print(TousDispo(BDPrix,panier))
print(TousDispo(BDPrix,panier2))
print(Total(BDPrix,panier))
        
        
