#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : scientifiques.py
#  auteur : Pascal CHAUVIN & Gregory MAUPU
#    date : 2020/12/15


ghopper = {
	'nom': 'Hopper',
	'prenom': 'grace',
	'annee_naissance': 1900,
	'annee_deces': 1992
}

ldevillers = {
	'nom': 'Devillers',
	'prenom': 'laurence',
	'annee_naissance': 1962
}

hypatia = {
	'nom': 'Hypatia',
	'annee_naissance': 370,
	'annee_deces': 415
}

abyron = {
	'nom': 'Byron',
	'prenom': 'ada',
	'annee_naissance': 1815,
	'annee_deces': 1852
}

sgermain = {
	'nom': 'Germain',
	'prenom': 'sophie',
	'annee_naissance': 1776,
	'annee_deces': 1831
}

mmirzakhani = {
	'nom': 'Mirzakhani',
	'prenom': 'maryam',
	'annee_naissance': 1977,
	'annee_deces': 2017
}

bhuberman = {
	'nom': 'Huberman',
	'prenom': 'barbara',
	'annee_naissance': 1939
}

mheafield = {
	'nom': 'Heafield',
	'prenom': 'margaret',
	'annee_naissance': 1936
}

bjennings = {
	'nom': 'Jennings',
	'prenom': 'betty',
	'annee_naissance': 1924
}

hkiesler = {
	'nom': 'Kiesler',
	'prenom': 'hedwig',
	'annee_naissance': 1914,
	'annee_deces': 2000
}

jclarke = {
	'nom': 'Clarke',
	'prenom': 'joan',
	'annee_naissance': 1917,
	'annee_deces': 2011
}

personnalites = []
	
personnalites.append(ghopper)
personnalites.append(hypatia)
personnalites.append(mheafield)
personnalites.append(ldevillers)
personnalites.append(jclarke)
personnalites.append(mmirzakhani)
personnalites.append(bhuberman)
personnalites.append(abyron)
personnalites.append(sgermain)
	

def question1(individus):
	"""
	Afficher les informations de la premiere personnalite de la liste 
	"individus".
	
	>>> question1(personnalites)
	['Hopper', 'grace', 1900, 1992]
	"""
	
	p = individus[0]
	return list(p.values())
	
	
def question2(individus):
	"""
	Parmi les personnalites de la liste "individus", est-il vrai que l'une 
	d'elles au moins porte le prenom "ada" ?
	
	>>> question2(personnalites)
	True
	"""
	
	for x in individus:
		if 'prenom' in x.keys():
			if x['prenom'] == 'ada':
				return True
	
	return False
	
	
def question3(individus):
	"""
	Donner la liste des noms des personnalites toujours en vie.
	
	La liste des noms sera rendue par ordre alphabetique.
	
	>>> question3(personnalites)
	['Heafield', 'Devillers', 'Huberman']
	"""
	
	noms = []
	
	for x in individus:
		if 'nom' in x.keys():
			if not ('annee_deces' in x.keys()):
				noms.append(x['nom'])
	
	return noms
	
	
def question4(individus):
	"""
	Parmi les personnalites de la liste "individus", donner les prenoms de 
	celles de moins de 20 avant la Seconde guerre mondiale.
	
	La liste des prenoms sera rendue par ordre alphabetique.
	
	>>> question4(personnalites)
	['barbara', 'margaret']
	"""
	
	prenoms = []
	
	for x in individus:
		if x['annee_naissance'] <= 1939 and 1939 - x['annee_naissance'] <= 20:
			prenoms.append(x['prenom'])
				
	return sorted(prenoms)
	
	
def question5(individus):
	"""
	Modifier la liste des personnalites en rendant sa majuscule a chaque prenom.
	
	>>> x = personnalites[0]
	>>> x['prenom']
	'grace'
	>>> question5(personnalites)
	>>> x = personnalites[0]
	>>> x['prenom']
	'Grace'
	"""
	
	for x in individus:
		if 'prenom' in x.keys():
			t = x['prenom']
			t = chr(ord(t[0]) + (ord('A') - ord('a'))) + t[1:len(t)]
			x['prenom'] = t


if __name__=="__main__":
	import doctest
	doctest.testmod(verbose=False)

# fin du fichier "scientifiques.py"

