
Le berceau du Web

Le chercheur britannique Tim Berners-Lee a inventé le World Wide Web en 1989, lorsqu’il travaillait au CERN. À l’origine, le projet a été conçu et développé pour que des scientifiques travaillant dans des universités et instituts du monde entier puissent s'échanger des informations instantanément.
Le CERN n’est pas un laboratoire isolé, mais le pôle de convergence d'une très vaste communauté, qui comprend plus de 17 000 scientifiques de plus de 100 pays. Bien que ces scientifiques passent en général une partie de leur temps au CERN, ils travaillent le plus souvent dans des universités et des laboratoires nationaux de leur pays d'origine. Il leur est donc essentiel de disposer d'outils de communication fiables.
L'idée de base du WWW était de combiner les technologies des ordinateurs personnels, des réseaux informatiques et de l'hypertexte pour créer un système d'information mondial, puissant et facile à utiliser.

